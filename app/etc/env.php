<?php
return [
  'backend' => [
    'frontName' => 'admin'
  ],
  'crypt' => [
    'key' => '71467bd58da197b50bf42c0eb8681d3c'
  ],
  'db' => [
    'table_prefix' => 'ws_',
    'connection' => [
      'default' => [
        'host' => 'localhost',
        'dbname' => 'wineseller',
        'username' => 'root',
        'password' => 'vinove',
        'active' => '1'
      ]
    ]
  ],
  'resource' => [
    'default_setup' => [
      'connection' => 'default'
    ]
  ],
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'developer',
  'session' => [
    'save' => 'files'
  ],
  'cache_types' => [
    'config' => 0,
    'layout' => 0,
    'block_html' => 0,
    'collections' => 0,
    'reflection' => 0,
    'db_ddl' => 0,
    'eav' => 0,
    'customer_notification' => 0,
    'config_integration' => 0,
    'config_integration_api' => 0,
    'full_page' => 0,
    'translate' => 0,
    'config_webservice' => 0
  ],
  'install' => [
    'date' => 'Sat, 26 May 2018 05:20:01 +0000'
  ]
];

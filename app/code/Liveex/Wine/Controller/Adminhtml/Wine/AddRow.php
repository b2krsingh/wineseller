<?php
/**
 * Liveex Wine List Controller.
 * @category  Liveex
 * @package   Liveex_Wine
 * @author    Vinove
 * @copyright Copyright (c) 2010-2018 Vinove Software Private Limited (https://vinove.com)
 * @license   https://vinove.com/license.html
 */
namespace Liveex\Wine\Controller\Adminhtml\Wine;

use Magento\Framework\Controller\ResultFactory;

class AddRow extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Webkul\Grid\Model\GridFactory
     */
    private $wineFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \Webkul\Grid\Model\GridFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Liveex\Wine\Model\WineFactory $wineFactory
    ) {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->wineFactory = $wineFactory;
    }

    /**
     * Mapped Grid List page.
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rowId = (int) $this->getRequest()->getParam('id');
        
        $rowData = $this->wineFactory->create();
        //echo '<pre>'; print_r($rowData); echo '</pre>'; die('mar');
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        if ($rowId) {
            $rowData = $rowData->load($rowId);
            $rowTitle = $rowData->getTitle();
            if (!$rowData->getEntityId()) {
                $this->messageManager->addError(__('row data no longer exist.'));
                $this->_redirect('wine/wine/rowdata');
                return;
            }
        }

        $this->coreRegistry->register('row_data', $rowData);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $title = $rowId ? __('Edit Row Data ').$rowTitle : __('Add Row Data');
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Liveex_Wine::add_row');
    }
}

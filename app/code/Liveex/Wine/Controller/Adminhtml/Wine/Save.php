<?php

/**
 * Liveex Admin Cagegory Map Record Save Controller.
 * @category  Liveex
 * @package   Liveex_Wine
 * @author    Vinove
 * @copyright Copyright (c) 2010-2018 Vinove Software Private Limited (https://vinove.com)
 * @license   https://vinove.com/license.html
 */
namespace Liveex\Wine\Controller\Adminhtml\Wine;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Webkul\Grid\Model\GridFactory
     */
    var $wineFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Webkul\Grid\Model\GridFactory $gridFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Liveex\Wine\Model\WineFactory $wineFactory
    ) {
        parent::__construct($context);
        $this->wineFactory = $wineFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if (!$data) {
            $this->_redirect('wine/wine/addrow');
            return;
        }
        try {
            $rowData = $this->wineFactory->create();
            $rowData->setData($data);
            if (isset($data['id'])) {
                $rowData->setEntityId($data['id']);
            }
            $rowData->save();
            $this->messageManager->addSuccess(__('Row data has been successfully saved.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->_redirect('wine/wine/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Liveex_Wine::save');
    }
}

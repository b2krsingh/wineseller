<?php
 
/**
 * Auction Wine Model.
 *
 * @category    Wine
 *
 * @author      Vinove Software Private Limited
 */
namespace Liveex\Wine\Model;
 
use Liveex\Wine\Api\Data\GridInterface;
 
class Wine extends \Magento\Framework\Model\AbstractModel
{
    
    
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'liveex_wine_records';
 
    /**
     * @var string
     */
    protected $_cacheTag = 'liveex_wine_records';
 
    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'liveex_wine_records';
    
    const ENTITY_ID = 'entity_id';
 
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Liveex\Wine\Model\ResourceModel\Wine');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }
 
    /**
     * Set EntityId.
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }
    
    
    /**
     * Get LWIN.
     *
     * @return varchar
     */
    public function getLwin()
    {
        return $this->getData(self::LWIN);
    }
 
    /**
     * Set LWIN.
     */
    public function setLwin($lwin)
    {
        return $this->setData(self::LWIN, $lwin);
    }
    
    
    /**
     * Get Reference.
     *
     * @return varchar
     */
    public function getReference()
    {
        return $this->getData(self::REFERENCE);
    }
 
    /**
     * Set Reference.
     */
    public function setReference($reference)
    {
        return $this->setData(self::REFERENCE, $reference);
    }
    
    /**
     * Get Wine Name.
     *
     * @return varchar
     */
    public function getWineName()
    {
        return $this->getData(self::WINE_NAME);
    }
 
    /**
     * Set Wine Name.
     */
    public function setWineName($title)
    {
        return $this->setData(self::WINE_NAME, $title);
    }
    
    /**
     * Get status.
     *
     * @return varchar
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }
 
    /**
     * Set status.
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
    
    /**
     * Get producer.
     *
     * @return varchar
     */
    public function getProducer()
    {
        return $this->getData(self::PRODUCER);
    }
 
    /**
     * Set producer.
     */
    public function setProducer($producer)
    {
        return $this->setData(self::PRODUCER, $producer);
    }
    
    /**
     * Get country.
     *
     * @return varchar
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }
 
    /**
     * Set country.
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }
    
    /**
     * Get region.
     *
     * @return varchar
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }
 
    /**
     * Set region.
     */
    public function setRegion($region)
    {
        return $this->setData(self::REGION, $region);
    }
    
    /**
     * Get sub region.
     *
     * @return varchar
     */
    public function getSubRegion()
    {
        return $this->getData(self::SUB_REGION);
    }
 
    /**
     * Set Sub region.
     */
    public function setSubRegion($sub_region)
    {
        return $this->setData(self::SUB_REGION, $sub_region);
    }
    
    /**
     * Get colour.
     *
     * @return varchar
     */
    public function getColour()
    {
        return $this->getData(self::COLOUR);
    }
 
    /**
     * Set colour.
     */
    public function setColour($colour)
    {
        return $this->setData(self::COLOUR, $colour);
    }
    
    /**
     * Get Type.
     *
     * @return varchar
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }
 
    /**
     * Set Type.
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
    
    /**
     * Get Designation.
     *
     * @return varchar
     */
    public function getDesignation()
    {
        return $this->getData(self::DESIGNATION);
    }
 
    /**
     * Set Designation.
     */
    public function setDesignation($designation)
    {
        return $this->setData(self::DESIGNATION, $designation);
    }
    
    /**
     * Get Classification.
     *
     * @return varchar
     */
    public function getClassification()
    {
        return $this->getData(self::CLASSIFICATION);
    }
 
    /**
     * Set Classification.
     */
    public function setClassification($classification)
    {
        return $this->setData(self::CLASSIFICATION, $classification);
    }
   
    /**
     * Get Date Added.
     *
     * @return varchar
     */
    public function getDateAdded()
    {
        return $this->getData(self::DATE_ADDED);
    }
 
    /**
     * Set Date Added.
     */
    public function setDateAdded($date_added)
    {
        return $this->setData(self::DATE_ADDED, $date_added);
    }
 
    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }
 
    /**
     * Set IsActive.
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
 
    
}
<?php

/**
 * Wine Wine Collection.
 *
 * @category  Liveex
 * @package   Liveex_Wine
 * @author    Vinove
 * @copyright Copyright (c) 2010-2018 Vinove Software Private Limited (https://vinove.com)
 * @license   https://vinove.com/license.html
 */
namespace Liveex\Wine\Model\ResourceModel\Wine;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Liveex\Wine\Model\Wine',
            'Liveex\Wine\Model\ResourceModel\Wine'
        );
    }
}

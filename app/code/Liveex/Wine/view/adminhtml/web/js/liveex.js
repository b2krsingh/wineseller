/*
 * @Author: zerokool - Nguyen Huu Tien
 * @Date:   2015-07-16 13:17:04
 * @Last Modified by:   zero
 * @Last Modified time: 2015-09-26 20:37:57
 */

'use strict';
require(['jquery'], function($) {
    $(document).ready(function() {
        setTimeout( function(){ 
            jQuery(".admin__data-grid-loading-mask").hide();
        }  , 10000 );
        
    });

});

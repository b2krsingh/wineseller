<?php
/**
 * Liveex_Wine Add New Row Form Admin Block.
 * @category    Liveex
 * @package     Liveex_Wine
 * @author      Vinove Software Private Limited
 *
 */
namespace Liveex\Wine\Block\Adminhtml\Wine\Edit;

/**
 * Adminhtml Add New Row Form.
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context,
     * @param \Magento\Framework\Registry $registry,
     * @param \Magento\Framework\Data\FormFactory $formFactory,
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
     * @param \Webkul\Grid\Model\Status $options,
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Liveex\Wine\Model\Status $options,
        array $data = []
    ) {
        $this->_options = $options;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('row_data');
        $form = $this->_formFactory->create(
            ['data' => [
                            'id' => 'edit_form',
                            'enctype' => 'multipart/form-data',
                            'action' => $this->getData('action'),
                            'method' => 'post'
                        ]
            ]
        );

        $form->setHtmlIdPrefix('wkgrid_');
        if ($model->getEntityId()) {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit Row Data'), 'class' => 'fieldset-wide']
            );
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Add Row Data'), 'class' => 'fieldset-wide']
            );
        }
        
        $fieldset->addField(
            'lwin',
            'text',
            [
                'name' => 'lwin',
                'label' => __('Lwin'),
                'id' => 'lwin',
                'title' => __('LWIN'),
                'class' => 'required-entry',
                'required' => true,
            ]
        );
        
        $fieldset->addField(
            'reference',
            'text',
            [
                'name' => 'reference',
                'label' => __('Reference'),
                'id' => 'reference',
                'title' => __('Reference'),
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'wine_name',
            'text',
            [
                'name' => 'wine_name',
                'label' => __('Wine Name'),
                'id' => 'wine_name',
                'title' => __('Wine Name'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'status',
            'text',
            [
                'name' => 'status',
                'label' => __('Status'),
                'id' => 'status',
                'title' => __('Status'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'producer',
            'text',
            [
                'name' => 'producer',
                'label' => __('Producer'),
                'id' => 'producer',
                'title' => __('Producer'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'country',
            'text',
            [
                'name' => 'country',
                'label' => __('Country'),
                'id' => 'country',
                'title' => __('Country'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'region',
            'text',
            [
                'name' => 'region',
                'label' => __('Region'),
                'id' => 'region',
                'title' => __('Region'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'sub_region',
            'text',
            [
                'name' => 'sub_region',
                'label' => __('Sub Region'),
                'id' => 'sub_region',
                'title' => __('Sub Region'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'colour',
            'text',
            [
                'name' => 'colour',
                'label' => __('Colour'),
                'id' => 'colour',
                'title' => __('colour'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'type',
            'text',
            [
                'name' => 'type',
                'label' => __('Type'),
                'id' => 'type',
                'title' => __('Type'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'designation',
            'text',
            [
                'name' => 'designation',
                'label' => __('Designation'),
                'id' => 'designation',
                'title' => __('Designation'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'classification',
            'text',
            [
                'name' => 'classification',
                'label' => __('Classification'),
                'id' => 'classification',
                'title' => __('Classification'),
                'class' => 'required-entry',
                'required' => false,
            ]
        );
        
        $fieldset->addField(
            'date_added',
            'date',
            [
                'name' => 'date_added',
                'label' => __('Date Added'),
                'date_format' => $dateFormat,
                'time_format' => 'H:mm:ss',
                'class' => 'validate-date validate-date-range date-range-custom_theme-from',
                'class' => 'required-entry',
                'style' => 'width:200px',
            ]
        );
        $fieldset->addField(
            'is_active',
            'select',
            [
                'name' => 'is_active',
                'label' => __('Status'),
                'id' => 'is_active',
                'title' => __('Status'),
                'values' => $this->_options->getOptionArray(),
                'class' => 'status',
                'required' => true,
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}

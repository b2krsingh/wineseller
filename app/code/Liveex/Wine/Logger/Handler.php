<?php
/**
 * Wine Logger Handler.
 * @category  Liveex
 * @package   Liveex_Wine
 * @author    Vinove
 * @copyright Copyright (c) 2010-2018 Vinove Software Private Limited (https://vinove.com)
 * @license   https://vinove.com/license.html
 */

namespace Liveex\Wine\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level.
     *
     * @var int
     */
    public $loggerType = Logger::INFO;

    /**
     * File name.
     *
     * @var string
     */
    public $fileName = '/var/log/grid.log';
}

<?php
/**
 * Liveex_Wine Grid Interface.
 *
 * @category    Liveex
 *
 * @author      Vinove Software Private Limited
 */
namespace Liveex\Wine\Api\Data;
 
interface WineInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const LWIN = 'lwin';
    const REFERENCE = 'reference';
    const WINE_NAME = 'wine_name';
    const STATUS = 'status';
    const PRODUCER = 'producer';
    const COUNTRY = 'country';
    const REGION = 'region';
    const SUB_REGION = 'sub_region';
    const COLOUR = 'colour';
    const TYPE = 'type';
    const DESIGNATION = 'designation';
    const CLASSIFICATION = 'classification';
    const DATE_ADDED = 'date_added';
    const IS_ACTIVE = 'is_active';
 
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId();
 
    /**
     * Set EntityId.
     */
    public function setEntityId($entityId);
    
    /**
     * Get LWIN.
     *
     * @return varchar
     */
    public function getLwin();
 
    /**
     * Set LWIN.
     */
    public function setLwin($lwin);
    
    /**
     * Get Reference.
     *
     * @return varchar
     */
    public function getReference();
 
    /**
     * Set Reference.
     */
    public function setReference($reference);
    
    /**
     * Get Wine Name.
     *
     * @return varchar
     */
    public function getWineName();
 
    /**
     * Set Wine Name.
     */
    public function setWineName($title);
 
    /**
     * Get Status.
     *
     * @return varchar
     */
    public function getStatus();
 
    /**
     * Set Status.
     */
    public function setStatus($status);
    
    /**
     * Get Producer.
     *
     * @return varchar
     */
    public function getProducer();
 
    /**
     * Set Producer.
     */
    public function setProducer($producer);
    
    /**
     * Get country.
     *
     * @return varchar
     */
    public function getCountry();
 
    /**
     * Set country.
     */
    public function setCountry($country);
    
    /**
     * Get Region.
     *
     * @return varchar
     */
    public function getRegion();
 
    /**
     * Set Region.
     */
    public function setRegion($region);
    
    /**
     * Get Sub Region.
     *
     * @return varchar
     */
    public function getSubRegion();
 
    /**
     * Set Sub Region.
     */
    public function setSubRegion($sub_region);
    
    /**
     * Get Colour.
     *
     * @return varchar
     */
    public function getColour();
 
    /**
     * Set Status.
     */
    public function setColour($colour);
    
    /**
     * Get Type.
     *
     * @return varchar
     */
    public function getType();
 
    /**
     * Set Type.
     */
    public function setType($type);
    
    /**
     * Get Designation.
     *
     * @return varchar
     */
    public function getDesignation();
 
    /**
     * Set Designation.
     */
    public function setDesignation($designation);
    
    /**
     * Get Classification.
     *
     * @return varchar
     */
    public function getClassification();
 
    /**
     * Set Classification.
     */
    public function setClassification($classification);
    
    
    
    
    
    
    
    
    
 
    /**
     * Get Date Added.
     *
     * @return varchar
     */
    public function getDateAdded();
 
    /**
     * Set Date Added.
     */
    public function setDateAdded($date_added);
 
    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive();
 
    /**
     * Set StartingPrice.
     */
    public function setIsActive($isActive);
 
}
<?php
/**
 * Liveex Module registration.
 * @category  Liveex
 * @package   Liveex_Wine
 * @author    Vinove
 * @copyright Copyright (c) 2010-2018 Vinove Software Private Limited (https://Vinove.com)
 * @license   https://vinove.com/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Liveex_Wine',
    __DIR__
);

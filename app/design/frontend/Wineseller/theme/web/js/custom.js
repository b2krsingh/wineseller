require([
  "jquery", "bxslider",
], function($, window, document, undefined){
    "use strict";
    $(document).ready(function($) {
        
        $('.search-box input[type="text"]').on("keyup input", function(){
            /* Get input value on change */
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings(".result");
            if(inputVal.length){
                $.get("search.php", {term: inputVal}).done(function(data){
                    // Display the returned data in browser
                    resultDropdown.html(data);
                });
            } else{
                resultDropdown.empty();
            }
        });

       
        
        
        $('.bxslider').bxSlider({
            mode: 'horizontal',
            infiniteLoop: true,
            auto: true,
            autoStart: true,
            autoDirection: 'next',
            autoHover: true,
            pause: 3000,
            autoControls: false,
            pager: true,
            pagerType: 'full',
            controls: true,
            captions: true,
            speed: 500
        });

        $(".dropdown").each(function(){
            $(this).click(function(){
                $(this).children(".dropdown-menu").toggle();
                $(this).siblings("li").children(".dropdown-menu").hide();
            })
        });

        $("#owl-demo").owlCarousel({
            loop:true,
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            items : 1, 
            autoplay:true,
            autoplayTimeout:3000,
        });
    });
});
// custom quanitity box
                jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fa fa-chevron-up"></i></div><div class="quantity-button quantity-down"><i class="fa fa-chevron-down"></i></div></div>').insertAfter('.quantity input');
                    jQuery('.quantity').each(function() {
                      var spinner = jQuery(this),
                        input = spinner.find('input[type="number"]'),
                        btnUp = spinner.find('.quantity-up'),
                        btnDown = spinner.find('.quantity-down'),
                        min = input.attr('min'),
                        max = input.attr('max');

                      btnUp.click(function() {
                        var oldValue = parseFloat(input.val());
                        if (oldValue >= max) {
                          var newVal = oldValue;
                        } else {
                          var newVal = oldValue + 1;
                        }
                        spinner.find("input").val(newVal);
                        spinner.find("input").trigger("change");
                      });

                      btnDown.click(function() {
                        var oldValue = parseFloat(input.val());
                        if (oldValue <= min) {
                          var newVal = oldValue;
                        } else {
                          var newVal = oldValue - 1;
                        }
                        spinner.find("input").val(newVal);
                        spinner.find("input").trigger("change");
                      });

                    });
require([
  "jquery", "bxslider",
], function($, window, document, undefined){
    "use strict";
    $(document).ready(function($) {
        $('.bxslider').bxSlider({
            mode: 'horizontal',
            infiniteLoop: true,
            auto: true,
            autoStart: true,
            autoDirection: 'next',
            autoHover: true,
            pause: 3000,
            autoControls: false,
            pager: true,
            pagerType: 'full',
            controls: true,
            captions: true,
            speed: 500
        });

        $(".dropdown").each(function(){
            $(this).click(function(){
                $(this).children(".dropdown-menu").toggle();
                $(this).siblings("li").children(".dropdown-menu").hide();
            })
        });

        $("#owl-demo").owlCarousel({
            loop:true,
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            items : 1, 
            autoplay:true,
            autoplayTimeout:3000,
        });
    });
});
